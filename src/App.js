

import { BrowserRouter, Route, Routes } from 'react-router-dom';
import routes from './routes';



import 'bootstrap/dist/css/bootstrap.css';
import './assets/css/style.css';
import './assets/css/responsive.css';

function App() {
  return (

    <BrowserRouter>
      <Routes>
  
        {
          routes.map((route, index) => {
              return(
                <Route path={route.path} element={<route.element/>} key={index}></Route>
              )
          })
        }
  
      </Routes>
    </BrowserRouter>
  );
}

export default App;
