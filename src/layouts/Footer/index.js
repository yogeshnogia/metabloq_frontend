import { Link } from "react-router-dom";
import logo from "../../assets/images/brand-logo/logo.png";

import social1 from "../../assets/images/icons/linkedin.png";
import social2 from "../../assets/images/icons/discord.png";
import social3 from "../../assets/images/icons/bear.png";
import social4 from "../../assets/images/icons/Medium.png";
import social5 from "../../assets/images/icons/Twitter.png";
import social6 from "../../assets/images/icons/Facebook.png";
import social7 from "../../assets/images/icons/Instagram.png";


function Footer(){
    return (
        <footer>
            <div className="container">
                <div className="row">
                    <div className="col-lg-4 footer-info">
                        <div className="foot-logo">
                            <img src={logo} alt="" />
                        </div>
                        <h4>About</h4>
                        <ul className="footer-links">
                            <li>
                                <Link to="/">Apply for Rewards</Link>
                            </li>
                            <li>
                                <Link to="/">Ecosystem</Link>
                            </li>
                            <li>
                                <Link to="/">Team</Link>
                            </li>
                        </ul>
                    </div>
                    <div className="col-lg-4 footer-info">
                        <h4>Contact</h4>
                        <ul className="footer-links">
                            <li>
                                <Link to="/">Info@metabloqs.com</Link>
                            </li>
                            <li>
                                <Link to="/">Press@metabloqs.com</Link>
                            </li>
                            <li>
                                <Link to="/">carrer@metabloqs.com</Link>
                            </li>
                        </ul>
                    </div>
                    <div className="col-lg-4 footer-info">
                        <ul className="footer-social">
                           <li>
                               <Link to="/"><img src={social1} alt="" /></Link>
                           </li>
                           <li>
                               <Link to="/"><img src={social2} alt="" /></Link>
                           </li>
                           <li>
                               <Link to="/"><img src={social3} alt="" /></Link>
                           </li>
                           <li>
                               <Link to="/"><img src={social4} alt="" /></Link>
                           </li>
                           <li>
                               <Link to="/"><img src={social5} alt="" /></Link>
                           </li>
                           <li>
                               <Link to="/"><img src={social6} alt="" /></Link>
                           </li>
                           <li>
                               <Link to="/"><img src={social7} alt="" /></Link>
                           </li>
                        </ul>

                        <ul className="footer-links">
                        <li>
                                <Link to="/">Metabloqs Whitepaper</Link>
                            </li>
                            <li>
                                <Link to="/">Xinfin Blockchain Network</Link>
                            </li>
                            <li>
                                <Link to="/">Terms & Conditions</Link>
                            </li>
                        </ul>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-12">
                        <p className="footer-bottom">&copy; 2022 Metabloqs. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default Footer;
