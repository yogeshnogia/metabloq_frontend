import brandLogo from "../../assets/images/brand-logo/logo.png";

import { Link } from "react-router-dom";
import React, { useState } from "react";

function Header(){

    const [isActive, setActive] = useState("true");

    const handleToggle = () => {
        setActive(!isActive);
    };

    return (
       <header className={isActive ? null : "active"}>
           <div className="container">
               <div className="brand-logo">
                   <img src={brandLogo} alt="brand-logo" />
               </div>
                <nav className="main-navigation">
                    <ul>
                        <li>
                            <Link to="/">White Paper</Link>
                        </li>
                        <li>
                            <Link to="/">Ecosystem</Link>
                        </li>
                        <li>
                            <Link to="/">Team</Link>
                        </li>
                    </ul>
                </nav>
                <div className="header-right">
                    <button>Register Now</button>
                </div>
                <button className="toggle_btn" onClick={handleToggle}>
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
           </div>
       </header>    

    )
}

export default Header;