import member1 from "../../assets/images/member-1.png";
import member2 from "../../assets/images/member-2.png";
import member3 from "../../assets/images/member-3.png";

function OurTeam(){
    return(
        <section className="our-team">
          <div className="container">
              <div className="section-title text-center">
                  <h4>The Team</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Enim molestie arcu est tempor. Nunc netus ipsum amet tristique mauris.</p>
              </div>

              <div className="row">
                  <div className="col-lg-4">
                      <div className="member-box">
                            <figure>
                                <img src={member1} alt="" />
                            </figure>
                            <figcaption>
                                <h4>Megha Shrestha</h4>
                                <h5>Founder & CEO</h5>
                                {/* <div className="social-links">
                                    <Link to="/"><img src={twitterIcon} alt="" /></Link>
                                    <Link to="/"><img src={linkedInIcon} alt="" /></Link>
                                </div> */}
                            </figcaption>
                      </div>
                  </div>
                  <div className="col-lg-4">
                      <div className="member-box">
                            <figure>
                                <img src={member2} alt="" />
                            </figure>
                            <figcaption>
                                <h4>Megha Shrestha</h4>
                                <h5>Founder & CEO</h5>
                            </figcaption>
                      </div>
                  </div>
                  <div className="col-lg-4">
                      <div className="member-box">
                            <figure>
                                <img src={member3} alt="" />
                            </figure>
                            <figcaption>
                                <h4>Megha Shrestha</h4>
                                <h5>Founder & CEO</h5>
                            </figcaption>
                      </div>
                  </div>
              </div>

              
              </div>
        </section>
    )
}

export default OurTeam;