import arrowIcon from "../../assets/images/icons/left-arrow.svg";

import banefitsImg1 from "../../assets/images/banefits-img-1.png";
import banefitsImg2 from "../../assets/images/banefits-img-2.png";
import banefitsImg3 from "../../assets/images/banefits-img-3.png";
import banefitsImg4 from "../../assets/images/banefits-img-4.png";

import { Link } from "react-router-dom";

function Benifits() {
  return (
    <section className="banefits">
      <div className="container">
        <div className="row">
          <div className="col-lg-4">
            <Link
              className="banefits-box banefits-box-type-1"
              to="/"
              style={{ backgroundImage: `url(${banefitsImg1})` }}
            >
              <div className="box-text">
                <h4>Premium <br /> Access</h4>
                <h2>First 500 users</h2>
                <span className="left-arrow">
                  <img src={arrowIcon} alt="" />
                </span>
              </div>
            </Link>
          </div>
          <div className="col-lg-4">
          <Link
              className="banefits-box banefits-box-type-2 mb-4"
              to="/"
              style={{ backgroundImage: `url(${banefitsImg2})` }}
            >
              <div className="box-text">
                  <div>
                  <h4>Avatar Looks</h4>
                    <h3>Customize</h3>
                  </div>                
                <span className="left-arrow">
                    <img src={arrowIcon} alt="" />
                </span>
              </div>
            </Link>
            <Link
              className="banefits-box banefits-box-type-2 banefits-box-type-3"
              to="/"
              style={{ backgroundImage: `url(${banefitsImg3})` }}
            >
              <div className="box-text">
                  <div>
                  <h4>Free NFT Land</h4>
                    <h3>Claim Now</h3>
                  </div>                
                <span className="left-arrow">
                    <img src={arrowIcon} alt="" />
                </span>
              </div>
            </Link>
          </div>

          <div className="col-lg-4">
          <Link
              className="banefits-box banefits-box-type-2 banefits-box-type-3 mb-4"
              to="/"
              style={{ backgroundImage: `url(${banefitsImg4})` }}
            >
              <div className="box-text">
                  <div>
                    <h4>Early Benefits</h4>
                  </div>                
                <span className="left-arrow">
                    <img src={arrowIcon} alt="" />
                </span>
              </div>
            </Link>

            <Link
              className="banefits-box banefits-box-type-2"
              to="/"
              style={{ backgroundImage: `url(${banefitsImg4})` }}
            >
              <div className="box-text">
                  <div>
                    <h4>XDC Tokens</h4>
                  </div>                
                <span className="left-arrow">
                    <img src={arrowIcon} alt="" />
                </span>
              </div>
            </Link>

          </div>
        </div>
      </div>
    </section>
  );
}

export default Benifits;
