import ctaImg from "../../assets/images/cta.png";
function Cta(){
    return(
        <section className="cta">
          <div className="cta-container">
            <div className="row">
                <div className="col-lg-6">
                    <div className="cta-text">
                        <h4>First Meta-city PARIS</h4>
                        <h3>Join Metabloqs and get rewards</h3>
                        <p>Be one of the first Metabloqs residents and get exclusive rewards: VIP Access,  NFT Land,  NFT assets  and XDC Tokens.</p>
                        <button className="button button-blue">Register NOW</button>
                    </div>
                </div>

                <div className="col-lg-6">
                <div className="cta-media">
                        <img src={ctaImg} alt="" />
                    </div>
                </div>
            </div>
          </div>
        </section>
    )
}

export default Cta;