import logoWhite from "../../assets/images/brand-logo/logo-black.png";

import galleryImg1 from "../../assets/images/gallery-img-1.png";

function Gallery(){
    return(
        <section className="gallery">
          <div className="container">
              <div className="gallery-section-title">
                    <img src={logoWhite} alt="" />
                    <h4>Metabloqs is a virtual world that.</h4>
                    <h2>
                        <span>Network.</span>
                        <span>Play.</span>
                        <span>Learn.</span>
                    </h2>
              </div>

              <ul className="gallery-list">
                    <li className="gallery-item clearfix">
                    <div className="gallery-text">
                        <p>Experience the  Virtual Paris Feel</p>
                    </div>
                    <div className="gallery-img">
                        <img src={galleryImg1} alt="" />
                    </div>
                    </li>
                    <li className="gallery-item clearfix">
                    <div className="gallery-text">
                        <p>Experience the  Virtual Paris Feel</p>
                    </div>
                    <div className="gallery-img">
                        <img src={galleryImg1} alt="" />
                    </div>
                    </li>
                    <li className="gallery-item clearfix">
                    <div className="gallery-text">
                        <p>Experience the  Virtual Paris Feel</p>
                    </div>
                    <div className="gallery-img">
                        <img src={galleryImg1} alt="" />
                    </div>
                    </li>
                    <li className="gallery-item clearfix">
                    <div className="gallery-text">
                        <p>Experience the  Virtual Paris Feel</p>
                    </div>
                    <div className="gallery-img">
                        <img src={galleryImg1} alt="" />
                    </div>
                    </li>
                </ul>


          </div>
        </section>
    )
}

export default Gallery;