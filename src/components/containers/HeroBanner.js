import banner1 from "../../assets/images/hero-banner-1.png";
import playIcon from "../../assets/images/icons/play-icon.svg";

function HeroBanner(){
    return(
        <section className="hero-bannner" style={{backgroundImage: banner1}} >
            <div className="container">
                <div className="banner-text">
                    <h1>Building technology for a healthier future</h1>
                    <button className="play-btn">Watch Trailer <img src={playIcon} alt="" /></button>
                    <button className="button button-blue">Register NOW</button>
                </div>
            </div>
        </section>
    )
}

export default HeroBanner;